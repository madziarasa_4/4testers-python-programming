# exercise 1 Print welcome message
def welcome_inscription(name, city):
    print(f"Witaj {name.capitalize()}! Miło Cię widzieć w naszym mieście: {city.capitalize()}!")

welcome_inscription("Michał", "Toruń")
welcome_inscription("beata", "gdynia")


# exercise 2 Generate email in 4testers.pl domain
def create_mail(name,surname):
    return f'{name.lower()}.{surname.lower()}@4testers.pl'

JNmail=create_mail("Janusz","Nowak")
print(JNmail)

BKmail=create_mail("Barbara","Kowalska")
print(BKmail)
