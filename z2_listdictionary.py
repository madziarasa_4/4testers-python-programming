# exercise 1 lista
emails = ['a@example.com', 'b@example.com']

print(len(emails))
print(emails[0])
print(emails[-1])
emails.append('c@example.com')
print(emails)

# exercise 2 słownik
friend = {
    'name': 'Agnes',
    'age': 47,
    'hobby': ['psychology', 'movies']
}

print(friend['age'])
friend['city'] = 'Warszawa'
friend['age'] = 46
print(friend)
del friend['city']
print(friend)
print(friend['hobby'][-1])


def get_employee_info(email):
    return {
        'emloyee_emil': email,
        'company': '4testers.pl'
    }


employee_1 = get_employee_info('robert@4terster.pl')
employee_2 = get_employee_info('adam@4terster.pl')

print(employee_1)
print(employee_2)
