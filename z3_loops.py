import uuid


def print_random_uuid(number_of_strings):
    for i in range(number_of_strings):
        print(str(uuid.uuid4()))


def print_numbers_from_20_to_30_multiplied_by_4():
    for number in range(10, 21):
        print(number * 4)


def print_temp_in_both_scales(list_of_Ctemps):
    for temp in list_of_Ctemps:
        Ftemp = temp * 1.8 + 32
        print(f'Celsius: {temp}, Fahrenheit:{Ftemp}')


def get_temp_higher_than_20(list_of_temp):
    filtered_temp = []
    for temp in list_of_temp:
        if temp > 20:
            filtered_temp.append(temp)
    return filtered_temp


def convert_temp(list_of_Ctemps):
    Ftemp = []
    for temp in list_of_Ctemps:
        Ftemp.append(round(temp * 1.8 + 32, 2))
    return Ftemp


if __name__ == '__main__':
    print_random_uuid(4)
    print_numbers_from_20_to_30_multiplied_by_4()

    temps_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
    print_temp_in_both_scales(temps_celsius)

    print(get_temp_higher_than_20(temps_celsius))

    print(convert_temp(temps_celsius))
