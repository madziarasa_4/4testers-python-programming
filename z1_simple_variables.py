my_name = "Magdalena"
my_age = 47
my_email = "madziarasa@poczta.onet.pl"

print(my_name)
print(my_age)
print(my_email)

friend_name = "Agnes"
friend_age = 47
friend_pets = 0
friend_has_driving_licence = True
friendship_time = 21.5

print(friend_name)
print(friend_age)
print(friend_pets)
print(friend_has_driving_licence)
print(friendship_time)
print(friend_name, friend_age, friend_pets, friend_has_driving_licence, friendship_time)
print(friend_name, friend_age, friend_pets, friend_has_driving_licence, friendship_time, sep="; ")
print("Friend's name:", friend_name, "\nFriend's age:", friend_age, "\nFriend's pets:", friend_pets, "\nFriend has driving licence:", friend_has_driving_licence, "\nFriend has driving licence:", friend_has_driving_licence, "\nFriendship time:", friendship_time)
def upper_word(word):
    return word.upper()

big_dog = upper_word("dog")
print(big_dog)