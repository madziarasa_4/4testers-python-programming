# exercise 1 function that returns the square of a number
def get_number_square(number):
    return number ** 2


zero_squared = get_number_square(0)
print(zero_squared)

sixteen_squared = get_number_square(16)
print(sixteen_squared)

floating_number_squared = get_number_square(2.55)
print(floating_number_squared)


# exercise 2 function that returns the volume of a cuboid a,b, c
def get_cuboid_volume(a, b, c):
    return a * b * c


volume_of_cuboid = get_cuboid_volume(3, 5, 7)
print(volume_of_cuboid)

# exercise 3 method that converts Celsius to Fahrenheit
def convert_C_toF(Celsius):
    return Celsius * 1.8 + 32

Celsius = 20
conver_Ctwenty = convert_C_toF(20)
print(f"Celsius = {Celsius}, Fahrenheit = {conver_Ctwenty}")