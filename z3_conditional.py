def print_word_length_description(name):
    if len(name) > 5:
        print(f'The name {name} is longer than 5 characters')
    else:
        print(f'The name {name} is 5 characters or shorter')


if __name__ == '__main__':
    long_name = 'Adrianna'
    short_name = 'Ada'

    print_word_length_description(long_name)
    print_word_length_description(short_name)

# exercise 1 słownik

def check_conditions(t, p):
    if t == 0 and p == 1013:
        return True
    else:
        return False

if __name__ == '__main__':


    print(check_conditions(0, 1013))
    print(check_conditions(1, 1014))
