import uuid


# exercise 1 sum and average function
def get_sum_of_numbers(numbers):
    return sum(numbers)


def get_avg_of_numbers(numbers):
    return sum(numbers) / len(numbers)


def get_avg_of_2months(a, b):
    return (a + b) / 2


if __name__ == '__main__':
    January = [-4, 1.0, -7, 2]
    February = [-13, -9, -3, 3]

    print(f'January sum:{get_sum_of_numbers(January)}')
    print(f'February sum:{get_sum_of_numbers(February)}')
    print(f'January avg:{get_avg_of_numbers(January)}')
    print(f'February avg:{get_avg_of_numbers(February)}')

    average_temperature_of_2months = get_avg_of_2months(get_avg_of_numbers(January), get_avg_of_numbers(February))
    print(f'JanFeb avg: {average_temperature_of_2months}')


# exercise 2 random login
def get_random_login(email):
    user_info = {
        'mail': email,
        'password': str(uuid.uuid4())
    }
    return user_info


print((f'user: {get_random_login("user@example.com")}'))
print((f'user: {get_random_login("buser@example.com")}'))


# exercise 3 function description player

def get_description_of_player(player_dictionary):
    print(
        f"The player {player_dictionary['nick']} is of type {player_dictionary['type']} and has {player_dictionary['points']} EXP"
    )


if __name__ == '__main__':
    player_one = {
        "nick": "maestro_54",
        "type": "warrior",
        "points": 3000
    }

    get_description_of_player(player_one)

if __name__ == '__main__':
    addresses = [
        {
            "city": "Warsaw",
            "street": "Rzeczpospolita",
            "house_number": "4",
            "post_code": "00-000"
        },
        {
            "city": "Lublin",
            "street": "Krakowska",
            "house_number": "4",
            "post_code": "11-11"
        },
        {
            "city": "Cracow",
            "street": "Floriańska",
            "house_number": "4",
            "post_code": "22-222"
        }
    ]

print(addresses[-1]["post_code"])
print(addresses[-1]["city"])
addresses[0]["street"] ="Sarmacka"
print(addresses)